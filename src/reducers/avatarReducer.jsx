import { types } from '../types/types';

const initialState = {
  actualAvatar: {
    body: null,
    head: null,
    mascota: null,
    fondo: null,
  },
  inventory: {
    body: [],
    head: [],
    mascota: [],
    fondo: [],
  },
  store: {
    body: [],
    head: [],
    mascota: [],
    fondo: [],
  },
};

export const avatarReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.setActualAvatar:
      return {
        ...state,
        actualAvatar: {
          ...state.actualAvatar,
          body: action.payload.body || state.actualAvatar.body,
          head: action.payload.head || state.actualAvatar.head,
          mascota: action.payload.mascota || state.actualAvatar.mascota,
          fondo: action.payload.fondo || state.actualAvatar.fondo,
        },
      };
    case types.setInventory:
      return {
        ...state,
        inventory: action.payload,
      };
    case types.setStore:
      return {
        ...state,
        store: action.payload,
      };

    case types.buyItem:
      return {
        ...state,
        store: action.payload,
      };
    default:
      return state;
  }
};

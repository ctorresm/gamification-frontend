import { types } from '../types/types';

const initialState = {
  ranking: [],
  active: false,
  showScore: false,
  score: 0,
  coins: 0,
  game: '',
};

export const gamesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.setGame:
      return {
        ...state,
        active: true,
        game: action.payload.game,
      };
    case types.removeGame:
      return {
        ...state,
        active: false,
        showScore: false,
        game: '',
      };
    case types.showScoreScreen:
      return {
        ...state,
        showScore: true,
      };
    case types.updateDataGame:
      return {
        ...state,
        score: action.payload.score,
        coins: action.payload.coins,
      };
    case types.setRanking:
      return {
        ...state,
        ranking: action.payload.ranking,
      };
    default:
      return state;
  }
};

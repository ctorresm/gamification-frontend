import { types } from '../types/types';

const initialState = {
  active: false,
  note: '',
};

export const notesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.setNote:
      return {
        ...state,
        active: true,
        note: action.payload.note,
      };
    case types.removeNote:
      return {
        ...state,
        active: false,
        note: '',
      };
    default:
      return state;
  }
};

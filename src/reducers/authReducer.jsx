import { types } from '../types/types';

export const authReducer = (state = {}, action) => {
  switch (action.type) {
    case types.login:
      return {
        ...state,
        user: action.payload.user,
      };
    case types.games:
      return {
        ...state,
        games: action.payload.games,
      };
    case types.lvlUpUser:
      return {
        ...state,
        user: {
          ...state.user,
          nivel_usuario: state.user.nivel_usuario + 1,
        },
      };
    case types.updateDataGame:
      return {
        ...state,
        user: {
          ...state.user,
          monedas_usuario: state.user.monedas_usuario + action.payload.coins,
          puntos_usuario: state.user.puntos_usuario + action.payload.score,
        },
      };
    case types.logout:
      return {};
    default:
      return state;
  }
};

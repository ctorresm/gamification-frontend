import { types } from '../types/types';

const initialState = {
  actualCoins: 0,
};

export const coinsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.updateCoins:
      localStorage.setItem('coins', action.payload);
      return {
        ...state,
        actualCoins: action.payload,
      };
    default:
      return state;
  }
};

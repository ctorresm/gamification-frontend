import React from "react";
import Button from "@material-ui/core/Button";
import ParticlesBg from "particles-bg";
import Box from "@material-ui/core/Box";
import { useHistory } from "react-router";

export const PortalScreen = () => {
  const history = useHistory();

  const handleClickStart = () => {
    history.push("/login");
  };

  return (
    <div className="portal">
      <ParticlesBg type="cobweb" bg={true} />
      <Box boxShadow={3}>
        <div className="portal__nav px-5 flex-v-center">
          <div>
            <img height="50px" src="images/logo.png" alt="" />
            <span className="portal__isotipo px-3">B Mates</span>
          </div>
          <div className="flex-spacer"></div>
        </div>
      </Box>

      <div className="portal__body flex-v-center">
        <div className="portal__box flex-v-center-left">
          <img src="images/bee.gif" className="px-5" alt="" />

          <div className="portal__box flex flex-column ml-3">
            <div className="portal__text">
              Aprende y diviértete junto con las mates
            </div>

            <Button variant="outlined" onClick={handleClickStart}>
              ¡Comienza!
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

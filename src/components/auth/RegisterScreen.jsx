import React from "react";
import { useForm } from "../../hooks/useForm";
import { useDispatch, useSelector } from "react-redux";
import { setError } from "../../actions/ui";
import { startRegisterWithEmailPassword } from "../../actions/auth";

import { TextField } from "@material-ui/core";
import { TiArrowBackOutline } from "react-icons/ti";
import { useHistory } from "react-router-dom";

import Alert from "@material-ui/lab/Alert";

export const RegisterScreen = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const { loading, msgError } = useSelector((state) => state.ui);

  const [formValues, handleInputChange] = useForm({
    name: "",
    user: "",
    password: "",
    password2: "",
  });

  const { user, password, name, password2 } = formValues;

  const handleRegister = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      dispatch(startRegisterWithEmailPassword(name, user, password, history));
    }
  };

  const isFormValid = () => {
    if (name.trim().length === 0) {
      dispatch(setError("El nombre es obligatorio"));
      return false;
    } else if (user.trim().length === 0) {
      dispatch(setError("El usuario no es válido"));
      return false;
    } else if (password.length < 5) {
      dispatch(setError("Las contraseñas deben ser mayores a 6 caractéres"));
      return false;
    } else if (password !== password2) {
      dispatch(setError("Las contraseñas no coinciden"));
      return false;
    } else {
      dispatch(setError(null));
      return true;
    }
  };

  const handleBackButton = () => {
    history.goBack();
  };

  return (
    <div className="auth__main">
      <div className="auth__options px-4 mt-4">
        <TiArrowBackOutline
          className="auth__backButton pointer"
          onClick={handleBackButton}
        />
        <div className="flex-spacer"></div>
        <button
          type="submit"
          className="btnLogin px-4 py-1"
          onClick={handleBackButton}
        >
          Ya tengo una cuenta
        </button>
      </div>
      <div className="auth__box-container  animate__animated animate__fadeIn">
        <h3 className="auth__title">Registro</h3>

        <form onSubmit={handleRegister} className="mt-4">
          <TextField
            label="¿Cuál es tu nombre?"
            variant="filled"
            name="name"
            className="auth__input mb-4"
            autoComplete="off"
            value={name}
            onChange={handleInputChange}
          />

          <TextField
            label="Usuario"
            variant="filled"
            name="user"
            className="auth__input my-3"
            autoComplete="off"
            value={user}
            onChange={handleInputChange}
          />

          <TextField
            label="Contraseña"
            variant="filled"
            type="password"
            name="password"
            className="auth__input mb-3"
            value={password}
            onChange={handleInputChange}
          />

          <TextField
            label="Repite la contraseña"
            variant="filled"
            type="password"
            name="password2"
            className="  mb-3"
            value={password2}
            onChange={handleInputChange}
          />

          <button
            disabled={loading}
            type="submit"
            className="btnLogin"
            onClick={handleRegister}
          >
            {loading ? "Loading..." : "Finalizar registro"}
          </button>

          <div className="mt-3">
            {msgError && <Alert severity="error">{msgError}</Alert>}
          </div>
        </form>
      </div>

      <div className="auth__background"> </div>

      <img className="auth__figure" src="/images/figura_registro.png" alt="" />
    </div>
  );
};

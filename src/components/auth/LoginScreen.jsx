import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { startLoginEmailPassword } from "../../actions/auth";
import { setError } from "../../actions/ui";
import { useForm } from "../../hooks/useForm";
import { TextField } from "@material-ui/core";
import { TiArrowBackOutline } from "react-icons/ti";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";

export const LoginScreen = () => {
  const dispatch = useDispatch();
  const { loading, msgError } = useSelector((state) => state.ui);
  const history = useHistory();

  const [formValues, handleInputChange] = useForm({
    email: "",
    password: "",
  });

  const { email, password } = formValues;

  const handleLogin = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      dispatch(startLoginEmailPassword(email, password));
    }
  };

  const handleBackButton = () => {
    history.push("/");
  };

  const handleForwardButton = () => {
    history.push("/register");
  };

  const isFormValid = () => {
    if (email.length < 5) {
      dispatch(setError("El usuario deberia tener mas de 5 letras"));
      return false;
    } else if (!password || password.length < 5) {
      dispatch(
        setError("La contraseña deberia tener como minimo 6 caracteres")
      );
      return false;
    } else {
      dispatch(setError(null));
      return true;
    }
  };

  return (
    <div className="auth__main">
      <div className="auth__options px-4 mt-4">
        <TiArrowBackOutline
          className="auth__backButton pointer"
          onClick={handleBackButton}
        />
        <div className="flex-spacer"></div>
        <button
          type="submit"
          className="btnLogin px-4 py-1"
          onClick={handleForwardButton}
        >
          Me registraré xd
        </button>
      </div>
      <div className="auth__box-container animate__animated animate__fadeIn">
        <h3 className="auth__title">Ingresar</h3>

        <form onSubmit={handleLogin} className="mt-4">
          <TextField
            label="Ingresa tu usuario :D"
            variant="filled"
            name="email"
            className="auth__input mb-3"
            autoComplete="off"
            value={email}
            onChange={handleInputChange}
          />

          <TextField
            label="Contraseña"
            variant="filled"
            type="password"
            placeholder="Password"
            name="password"
            className="auth__input  mb-3"
            value={password}
            onChange={handleInputChange}
          />

          <button disabled={loading} type="submit" className="btnLogin">
            {loading ? "Loading..." : "Iniciar sesión"}
          </button>

          <div className="mt-3">
            {msgError && <Alert severity="error">{msgError}</Alert>}
          </div>
        </form>
      </div>

      <div className="auth__background"> </div>
    </div>
  );
};

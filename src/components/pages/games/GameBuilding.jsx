import React from "react";
import { IoBuildOutline } from "react-icons/io5";

export const GameBuilding = () => {
  return (
    <div className="text-center">
      <h1 style={{ color: "white" }}>
        <IoBuildOutline />
        <IoBuildOutline />
        <IoBuildOutline />

        <span className="mx-3">En construcción...</span>

        <IoBuildOutline />
        <IoBuildOutline />
        <IoBuildOutline />
      </h1>
      <hr />
    </div>
  );
};

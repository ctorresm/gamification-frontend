import React from 'react'

export const NoteSuma = () => {
    return (
        <div className="flex-column suma__note">
            <h1>Las sumas</h1>
            <div className="mt-4 fs-20">
                Las sumas y restas son las primeras operaciones matemáticas que aprendemos; algunos niños incluso aprenden antes de comenzar la escuela primaria. Aprender a sumar y restar es sencillo, si tenemos en cuenta que a los niños pequeños les cuesta más entender conceptos abstractos que reales: si les mostramos cuatro manzanas y luego añadimos una explicando que ahora son cinco, lo entenderán mejor que si decimos 4+1 es igual a 5.
                En este post vamos a comenzar desde lo muy básico, para luego pasar a operaciones de sumas y restas llevando; vamos a explicarlo todo paso a paso, y vamos a dejaros ejercicios para practicar lo aprendido.
            </div>

            <div className="suma__subNote my-4 fw-700">
                Sumar es juntar dos o más cosas en un grupo, para saber cuántas hay en total
            </div>

            <div className="mt-2 fw-800 fs-18">
                ¡Veamos un ejemplo!
            </div>

            <img className="mt-2" src="https://www.pequeocio.com/wp-content/uploads/2019/04/sumas-y-restas.jpg" alt="" />

            <p className="mt-3 fs-20">
                Si tengo 2 manzanas verdes y 3 manzanas rojas, y quiero saber cuántas manzanas tengo en total, junto todas las manzanas en un solo cesto y las cuento: tengo 5 manzanas en total, por lo tanto 2 + 3  es igual a 5.
                También podemos representar la suma en la línea de números, e ir saltando de un número a otro para hacer la suma.
            </p>

            <div className="mt-3 fw-800 fs-18">
                Por ejemplo:
            </div>

            <img className="mt-2" src="https://www.pequeocio.com/wp-content/uploads/2019/04/sumas-restas.jpg" alt="" />

            <p className="mt-3 fs-20">
                Un pajarito salta hasta el número 3. Luego hace otros 2 saltos, y llega al número 5. Por lo tanto, 2 + 3  es igual a 5.
            </p>

            <div className="mt-3 mb-5 fs-18">
                <span className="fs-30 me-3">👉</span>
                Los números que se suman dentro de una operación se llaman <strong>SUMANDOS</strong>
            </div>

        </div>
    )
}

import React, { Fragment, useEffect, useState } from 'react'
import Intervalo from './Intervalo';
import { RiQuestionFill } from 'react-icons/ri'
import { Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { AlertTitle } from '@material-ui/lab';
import GamePuntuacion from '../../components/GamePuntuacion';
import { useDispatch, useSelector } from 'react-redux';
import { updateGameScore } from '../../../../actions/game';

const generateProblems = (qtyProblems = 10) => {
    const problems = [];

    for (let i = 0; i < qtyProblems; i++) {

        const numRandom = Math.floor(Math.random() * 6) + 1
        const answRandom = Math.floor(Math.random() * 6) + 7
        const answer = answRandom - numRandom;
        const rnd = Math.random() < 0.5;
        const alt = [];

        do {
            const newRandomNumber = Math.floor(Math.random() * answRandom) + numRandom;
            if (!alt.includes(newRandomNumber)) {
                alt.push(newRandomNumber);
            }
        } while (alt.length !== 4);

        if (!alt.includes(answer)) {
            const randomPos = Math.floor(Math.random() * 4);
            alt[randomPos] = answer;
        }

        problems.push({
            fn: rnd ? numRandom : null,
            sn: !rnd ? numRandom : null,
            tn: answRandom,
            answer,
            alternatives: alt
        })
    }
    return problems;
}

const GameSumasBasicas = () => {

    const [actualProblem, setActualProblem] = useState(0);
    const [problems, setProblems] = useState([]);
    const [openAlert, setOpenAlert] = useState(false)
    const [score, setScore] = useState(0);
    const [racha, setRacha] = useState(0);
    const [disabledButtons, setdisabledButtons] = useState(false)

    const auth = useSelector(state => state.auth);
    const game = useSelector(state => state.game);

    const dispatch = useDispatch();
    const problemasToRender = generateProblems();

    useEffect(() => {
        setProblems(problemasToRender);
        if (actualProblem + 1 === problems.length - 1) {
            setActualProblem(0);
        } else {
            setActualProblem(a => a + 1)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const nextProblem = () => {
        if (actualProblem + 1 === problems.length - 1) {
            setActualProblem(0);
        } else {
            setActualProblem(actualProblem + 1)
        }
    }

    const clickAnswerHandler = (value) => {

        if (value === problems[actualProblem].answer) {
            setOpenAlert(false)
            setRacha(racha + 1);
            setScore(score + 1000 + 20 * racha)
            nextProblem();
        } else {
            setOpenAlert(true)
            setRacha(0);
            if (score !== 0) {
                setScore(score - 500);
            }
            setTimeout(() => {
                setActualProblem(actualProblem + 1)
                nextProblem();
                setOpenAlert(false)
            }, 6000);

        }
    }

    const handleAction = (e) => {
        dispatch(updateGameScore(auth.user, score, score / 10, 1));
        setdisabledButtons(true);
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenAlert(false);
    };

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const RenderButton = React.memo(() => {
        return (
            <div className="suma__button_section">
                {
                    problems[actualProblem].alternatives.map(
                        (el, index) => (
                            <div className="button pointer" key={index}>
                                <button onClick={() => clickAnswerHandler(el)} disabled={disabledButtons}>{el}</button>
                            </div>
                        )
                    )
                }
            </div>
        )
    })

    return (
        <Fragment>
            {
                !game.showScore && problems.length > 0 && (
                    <div>

                        <div className="suma__top flex-v-center">
                            <div>
                                <button ></button>
                            </div>
                            <div className="flex-spacer"></div>
                            <div>
                                <div>Puntuación:
                                    <span className="fs-24 mx-3"> {score}</span>
                                </div>
                                <div className="fs-16">
                                    Racha:
                                        <span className={`fs-24 mx-3 ${racha > 5 ? 'fire-text' : ''}`}>
                                        X{racha}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="flex-v-center">
                            <h1 style={{ color: 'red' }}>
                                <Intervalo seconds={30} onAction={handleAction} />
                            </h1>
                        </div>

                        <div className="flex-v-center-column">
                            <div className="problem">
                                <div className="suma__cajon_problemas d-flex animate__animated animate__bounce">
                                    <div className={`suma__box suma__box_1 
                                    ${problems[actualProblem].fn ? '' : 'suma__outlined animate__animated animate__headShake animate__infinite'}`}>
                                        {problems[actualProblem].fn || <RiQuestionFill style={{ fontSize: '50px' }} />}
                                    </div>

                                    <div className="suma__box suma__plus">
                                        +
                                    </div>

                                    <div className={`suma__box suma__box_2 
                                    ${problems[actualProblem].sn ? '' : 'suma__outlined animate__animated animate__headShake animate__infinite'}`}>
                                        {problems[actualProblem].sn || <RiQuestionFill style={{ fontSize: '50px' }} />}
                                    </div>

                                    <div className="suma__box suma__equal">
                                        =
                                    </div>

                                    <div className="suma__box suma__box_3">
                                        {problems[actualProblem].tn}
                                    </div>
                                </div>
                            </div>

                            <RenderButton />

                        </div>
                        <Snackbar open={openAlert} autoHideDuration={100000} onClose={handleClose}>
                            <Alert onClose={handleClose} severity="warning" className="fs-24 w-100">
                                <AlertTitle className="fs-20 w-100">¡Te has equivocado!</AlertTitle>
                                {`La respuesta era ${problems[actualProblem].answer} :c`}
                                <br />
                                {`${problems[actualProblem].fn || problems[actualProblem].sn} + ${problems[actualProblem].answer} 
                                = ${problems[actualProblem].tn}, por favor presiona la respuesta correcta,`}
                                <strong> ¡El tiempo corre xd!</strong>
                            </Alert>
                        </Snackbar>

                        {/* <Alert
                            action={
                                <IconButton
                                    aria-label="close"
                                    color="inherit"
                                    size="small"
                                    onClick={() => {
                                        setOpen(false);
                                    }}
                                >
                                    <CloseIcon fontSize="inherit" />
                                </IconButton>
                            }
                        >
                            Close me!
                        </Alert> */}

                    </div>
                )
            }
            {
                game.showScore && (<GamePuntuacion />)
            }
        </Fragment >
    )
}

export default React.memo(GameSumasBasicas);
import React, { Fragment, useEffect, useRef } from 'react'
import { Power2 } from 'gsap'
import { GameSuma10 } from './GameSuma10';
import { gsap } from 'gsap'
import { GameSuma20 } from './GameSuma20';
import { useDispatch, useSelector } from 'react-redux';
import { setGame } from '../../../../actions/game';
import GameSumasBasicas from './GameSumasBasicas';

const GameSuma = () => {

    const timeLine = useRef(null);
    let btn1 = useRef(null);
    let btn2 = useRef(null);
    let btn3 = useRef(null);

    const dispatch = useDispatch()

    const selector = useSelector(state => state.game)

    useEffect(() => {
        if (btn1.current) {
            timeLine.current = gsap.timeline({ paused: false });
            timeLine.current.to(btn1.current, 1, {
                opacity: 1,
                y: 1080,
                ease: Power2.easeOut
            },0);
            timeLine.current.to(btn2.current, 1, {
                opacity: 1,
                y: 1080,
                ease: Power2.easeOut
            },.25);
            timeLine.current.to(btn3.current, 1, {
                opacity: 1,
                y: 1080,
                ease: Power2.easeOut
            },.50);
        }
    }, [btn1, btn2, btn3])

    const games = [
        { id: 1, component: GameSumasBasicas },
        { id: 2, component: GameSuma10 },
        { id: 3, component: GameSuma20 },
    ]

    const clickSumaBasicaHandler = (id) => {
        dispatch(setGame(id))
        timeLine.current = gsap.timeline({ paused: true });
    }

    const ShowGame = games.filter(g => g.id === selector.game)[0]?.component || GameSuma10;

    return (
        <div className="game__suma flex-v-center-column">

            <h1>Las sumas</h1>

            <hr className="w-100 bg-danger" />

            <div className="game__suma__body mt-4 mat-elevation-z2">

                {!selector.active &&

                    (<div className="game__sections">

                        <div className="suma__option" ref={btn1}>
                            <button onClick={() => clickSumaBasicaHandler(1)}>
                                <img src="https://cdn2.iconfinder.com/data/icons/web-and-ui-21/20/1029-512.png" alt="" />
                            </button>
                            <span>Sumas básicas</span>
                        </div>

                        <div className="suma__option" ref={btn2}>
                            <button onClick={() => clickSumaBasicaHandler(2)}>
                                <img src="https://cdn2.iconfinder.com/data/icons/web-and-ui-21/20/1029-512.png" alt="" />
                            </button>
                            <span>Sumas hasta 10</span>
                        </div>

                        <div className="suma__option" ref={btn3}>
                            <button onClick={() => clickSumaBasicaHandler(3)}>
                                <img src="https://cdn2.iconfinder.com/data/icons/web-and-ui-21/20/1029-512.png" alt="" />
                            </button>
                            <span>Sumas hasta 20</span>
                        </div>

                    </div>)
                }

                {
                    selector.active &&
                    (
                        <Fragment>
                            <ShowGame />
                        </Fragment>
                    )
                }

            </div>

        </div>
    )
}

export default React.memo(GameSuma);

import React, { useEffect, useState } from 'react'

const Intervalo = (props) => {

    const { seconds, onAction } = props;

    let [second, setSecond] = useState(seconds);

    useEffect(() => {
        const timer = setInterval(() => {
            if (second > 0) {
                setSecond(s => s = s - 1)
            } else {
                clearInterval(timer)
                onAction('ok')
            }
        }, 1000);
        return () => clearInterval(timer);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [second])

    return (
        <div>
            {second}
        </div>
    )
}

export default Intervalo

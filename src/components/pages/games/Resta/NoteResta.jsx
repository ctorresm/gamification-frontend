import React from 'react'

export const NoteResta = () => {
    return (
        <div className="flex-column suma__note">
            <h1>Las restas o sustracciones</h1>
            <div className="mt-4 fs-20">
                En una resta,  el primero de los dos números que intervienen y es la cantidad de la que debe restarse otra se llama MINUENDO. El segundo número que debe restarse al primero se llama SUSTRAENDO.
            </div>

            <div className="suma__subNote my-4 fw-700">
                Restar es quitar una cierta cantidad a otra que ya teníamos
            </div>

            <div className="mt-2 fw-800 fs-18">
                ¡Veamos un ejemplo!
            </div>

            <img className="mt-2" src="https://www.pequeocio.com/wp-content/uploads/2019/04/restas.jpg" alt="" />

            <p className="mt-3 fs-20">
                Si tengo 5 manzanas en una cesta, y quito 2, dentro de la cesta me quedarán 3 manzanas. es decir que 5 menos 2 es igual a 3.
                Como hemos visto con las sumas, también podemos usar la línea de números para restar.
            </p>

            <div className="mt-3 fw-800 fs-18">
                Por ejemplo:
            </div>

            <img className="mt-2" src="https://www.pequeocio.com/wp-content/uploads/2019/04/sumas-y-restas-primaria.jpg" alt="" />

            <p className="mt-3 fs-20">
                Un pajarito hace 5 saltos y llega al número 5, luego hace 2 saltos hacia atrás, y por lo tanto llega al número 3.
            </p>

            <div className="mt-3 mb-5 fs-18">
                <span className="fs-30 me-3">👉</span>
                En una resta,  el primero de los dos números que intervienen y es la cantidad de la que debe restarse otra se llama MINUENDO.
                El segundo número que debe restarse al primero se llama SUSTRAENDO.
            </div>

        </div>
    )
}

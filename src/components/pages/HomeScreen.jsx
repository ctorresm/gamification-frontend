import React, { useEffect, useState } from 'react';
import HomeScreenAvatar from './components/HomeScreenAvatar';
import HomeScreenProfile from './components/HomeScreenProfile';
import { Grid, Modal } from '@material-ui/core';
import HomeContent from './components/HomeContent';
import ParticlesBg from 'particles-bg';
import { useDispatch, useSelector } from 'react-redux';
import { removeGame } from '../../actions/game';
import { removeNote } from '../../actions/notes';
import { makeStyles } from '@material-ui/core/styles';
import { hideLevelUpScreen } from '../../actions/ui';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const HomeScreen = () => {
  const config = {
    num: [4, 7],
    rps: 0.1,
    radius: [5, 40],
    life: [1.5, 3],
    v: [2, 3],
    tha: [-40, 40],
    alpha: [0.6, 0],
    scale: [0.1, 0.4],
    position: 'all',
    random: 15,
  };
  const classes = useStyles();
  const dispatch = useDispatch();
  const [modalStyle] = React.useState(getModalStyle);
  const uiState = useSelector((state) => state.ui);
  const [state, setState] = useState(false);

  dispatch(removeGame());
  dispatch(removeNote());

  const handleClose = () => {
    dispatch(hideLevelUpScreen());
  };

  useEffect(() => {
    if (uiState.lvlUp) {
      setState(true);
    } else {
      setState(false);
    }
  }, [uiState]);

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">¡Haz subido de nivel!</h2>
      <div id="simple-modal-description">
        Aquí tienes algunos regalos como recompensa!
        <div className="mt-3 text-center">+ 800 chocoins</div>
        <div className="mt-3 text-center">Alas navideñas</div>
      </div>
    </div>
  );
  return (
    !uiState.loading && (
      <Grid
        container
        alignContent="center"
        justify="center"
        className="p-4 gridHome"
      >
        {!uiState.loading && (
          <ParticlesBg type="custom" config={config} bg={true} />
        )}

        <Modal
          open={state}
          onClose={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          {body}
        </Modal>

        {!uiState.loading && (
          <Grid container item xs={12} sm={12} md={8} className="z2">
            <HomeContent />
          </Grid>
        )}

        <Grid container item xs={12} sm={12} md={4} direction="column">
          <Grid item className="home__section m-4">
            <HomeScreenProfile />
          </Grid>
          <Grid item className="home__section m-4 py-4">
            <HomeScreenAvatar mode={1} />
          </Grid>
          <Grid item className="home__section m-4 p-4">
            <h4>Medallas obtenidas</h4>
            <span>
              En esta sección podras observar las medallas que has obtenido a lo
              largo del curso. <b> ¡Haste con todas!</b>
            </span>
          </Grid>
        </Grid>
      </Grid>
    )
  );
};

export default HomeScreen;

import React, { useRef, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { removeGame } from "../../actions/game";
import { TweenMax, Power2 } from "gsap";
import { GameResta } from "./games/GameResta";
import { FaWindowClose } from "react-icons/fa";
import { GameBuilding } from "./games/GameBuilding";
import GameSuma from "./games/Suma/GameSuma";

export const GameScreen = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  let game_body = useRef(null);

  useEffect(() => {
    TweenMax.to(game_body, 0.75, {
      opacity: 1,
      y: 1080,
      ease: Power2.easeOut,
    });
  }, [game_body]);

  const goBackClickHandler = () => {
    dispatch(removeGame());
    history.goBack();
  };

  const games = [
    { id: 1, component: GameSuma },
    { id: 2, component: GameResta },
  ];

  const GameToRender =
    games.filter((g) => g.id === Number(props.match.params.id))[0]?.component ||
    GameBuilding;

  return (
    <div className="game__body" ref={(el) => (game_body = el)}>
      <div className="flex-v-column w-100">
        <div className="flex-v-center px-4 mt-3">
          <div className="flex-spacer"></div>
          <FaWindowClose
            className="auth__backButton pointer"
            onClick={goBackClickHandler}
          />
        </div>
      </div>
      <GameToRender />
    </div>
  );
};

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getRanking } from '../../actions/game';
import HomeScreenAvatar from './components/HomeScreenAvatar';
import { makeStyles } from '@material-ui/core/styles';
import { Modal } from '@material-ui/core';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '4px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4, 4, 3),
    textAlign: 'center',
  },
}));

const RankingScreen = () => {
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);

  const [users, setUsers] = useState([]);

  const [state, setstate] = useState(null);

  const game = useSelector((state) => state.game);
  const userState = useSelector((state) => state);

  const [avatar, setAvatar] = useState(null);

  const handleClick = (avatar) => {
    console.log(avatar);
    setAvatar(avatar);
    setOpen(true);
  };

  const dispatch = useDispatch();

  useEffect(() => {
    setUsers(game.ranking);
  }, [game]);

  const onClickHandler = (id) => {
    setstate(id);
    dispatch(getRanking(id, userState.auth.user.id_usuario));
  };

  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
    setAvatar(null);
  };

  const RenderTableBody = React.memo(() => {
    return (
      <>
        <tbody>
          {users.length > 0 &&
            users.map((el, index) => (
              <tr className="pointer ranking__wrap_user" key={index}>
                <td
                  className="text-center avatar__preview_rank"
                  style={{ zoom: '0.15' }}
                  onClick={() => handleClick(el)}
                >
                  <HomeScreenAvatar mode={0} item={el.item} />
                </td>

                <td className="index ranking__fila text-center">
                  {index === 0 && (
                    <img
                      src="https://www.flaticon.es/svg/static/icons/svg/2583/2583381.svg"
                      width="52px"
                      alt=""
                    />
                  )}
                  {index === 1 && (
                    <img
                      src="https://www.flaticon.es/svg/static/icons/svg/2583/2583350.svg"
                      width="52px"
                      alt=""
                    />
                  )}
                  {index === 2 && (
                    <img
                      src="https://www.flaticon.es/svg/static/icons/svg/2583/2583448.svg"
                      width="52px"
                      alt=""
                    />
                  )}
                  {index > 2 && index < 10 && index + 1}
                  {index === 10 && 'Mi puesto: ' + el.puesto}
                </td>
                <td className="ranking__fila text-center">
                  {el.nombre_usuario}
                </td>
                <td className="ranking__fila text-center">{el.nick_usuario}</td>
                <td className="ranking__fila text-center">
                  {el.nivel_usuario}
                </td>
                <td className="ranking__fila text-center">
                  {el.puntos_ganados}
                </td>
              </tr>
            ))}
        </tbody>
      </>
    );
  });

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">{avatar?.nombre_usuario}</h2>
      <h6>Puesto {avatar?.puesto}</h6>

      <h5 className="mt-3">{avatar?.puntos_ganados}</h5>
      <HomeScreenAvatar mode={0} item={avatar?.item} />
    </div>
  );

  return (
    <div className="ranking">
      <div className="mx-5 pb-3 pt-5 flex-v-center fw-600 fs-22 z2">
        Selecciona el tema
      </div>
      <div className="ranking__btnSection">
        <button
          className={state === 1 ? `bg-success` : ''}
          onClick={() => onClickHandler(1)}
        >
          Sumas Básicas
        </button>
        <button
          className={state === 2 ? `bg-success` : ''}
          onClick={() => onClickHandler(2)}
        >
          Restas Básicas
        </button>
        <button
          className={state === 3 ? `bg-success` : ''}
          onClick={() => onClickHandler(3)}
        >
          Sumas Básicas xd
        </button>
      </div>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>

      <div className="ranking__list flex-v-center flex-column mt-4">
        <table className="ranking__table table-striped">
          <thead>
            <tr className="text-center">
              <th scope="col">Avatar</th>
              <th scope="col">Posición</th>
              <th scope="col">Nombres</th>
              <th scope="col">Nickname</th>
              <th scope="col">Nivel</th>
              <th scope="col">Puntos</th>
            </tr>
          </thead>
          <RenderTableBody />
        </table>
        {users.length === 0 && (
          <div className="mt-5 flex-v-center flex-column fs-20">
            <img
              src="https://friendlystock.com/wp-content/uploads/2018/06/2-monkey-holding-empty-sign-cartoon-clipart.jpg"
              width="80px"
              alt=""
            />
            <span className="mt-3">Para iniciar da click en algun tema</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default RankingScreen;

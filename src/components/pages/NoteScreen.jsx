import React, { Fragment } from "react";
import { TiArrowBackOutline } from "react-icons/ti";
import { useDispatch, useSelector } from "react-redux";
import { removeNote } from "../../actions/notes";
import { NoteResta } from "./games/Resta/NoteResta";
import { NoteSuma } from "./games/Suma/NoteSuma";

const NoteScreen = () => {
  const noteState = useSelector((state) => state.note);
  const dispatch = useDispatch();

  const goBackClickHandler = () => {
    dispatch(removeNote());
  };

  const notes = [
    { id: 1, component: NoteSuma },
    { id: 2, component: NoteResta },
  ];

  const defaultNote = () => (
    <>
      <strong className="fs-22"> Bienvenido! </strong>
      <div className="mt-3">
        En esta unidad se veran los temas básicos y la teoría correspondiente al
        tema elegido.
      </div>
    </>
  );

  const NoteToRender =
    notes.filter((n) => n.id === noteState.note)[0]?.component || defaultNote;

  return (
    <Fragment>
      <div className="w-100" style={{ position: "relative" }}>
        <div className="flex-v-center">
          <TiArrowBackOutline
            className="auth__backButton pointer"
            onClick={goBackClickHandler}
          />
          <div className="flex-spacer"></div>
        </div>

        <NoteToRender />
      </div>
    </Fragment>
  );
};

export default NoteScreen;

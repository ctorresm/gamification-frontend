import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getStore } from '../../actions/avatar';
import { Grid, Paper } from '@material-ui/core';
import HomeScreenAvatar from './components/HomeScreenAvatar';
import HomeScreenProfile from './components/HomeScreenProfile';
import StoreComponent from './components/StoreComponent';

const StoreScreen = () => {
  const dispatch = useDispatch();
  const [itemsUpdated, setItemsUpdated] = useState(null);

  useEffect(() => {
    dispatch(getStore());
  }, [dispatch]);

  return (
    <div className="body__stire">
      <Grid container className="px-2">
        <Grid
          item
          container
          lg={4}
          alignContent="center"
          justify="flex-start"
          direction="column"
          alignItems="center"
          className="py-4 px-2"
          style={{
            zIndex: '1000',
          }}
        >
          <Paper
            elevation={7}
            className="py-2 px-4  fs-22 flex-v-center fw-600"
            style={{ background: 'white', color: '#111' }}
          >
            🤘 Mi Avatar 🤘
          </Paper>
          <HomeScreenAvatar mode={0} item={itemsUpdated} />
          <Paper
            className="px-5 mt-2 pb-1 flex-column"
            elevation={2}
            style={{
              borderRadius: '8px',
              background: 'white',
              width: '15rem',
            }}
          ></Paper>
          <Paper elevation={2} className="wrapper_avatar mt-4">
            <HomeScreenProfile storeMode={true} />
          </Paper>
        </Grid>
        <Grid
          item
          container
          lg={8}
          alignContent="center"
          direction="column"
          className="p-4"
        >
          <StoreComponent
            itemsUpdated={itemsUpdated}
            setItemsUpdated={setItemsUpdated}
          ></StoreComponent>
        </Grid>
      </Grid>
    </div>
  );
};

export default React.memo(StoreScreen);

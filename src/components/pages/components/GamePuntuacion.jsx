import React, { useEffect, useRef, useState } from "react";
import { gsap, Power2 } from "gsap";
import { BiCoin } from "react-icons/bi";
import { useDispatch, useSelector } from "react-redux";
import { lvlUpUser } from "../../../actions/auth";
import { showLevelUpScreen } from "../../../actions/ui";

const GamePuntuacion = () => {
  const timeLine = useRef(null);
  const gameState = useSelector((state) => state.game);
  const authState = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  let [second, setSecond] = useState(0);

  let info = useRef(null);
  let scoreBox = useRef(null);
  let coinsBox = useRef(null);

  useEffect(() => {
    const user = authState.user;
    if (user.nivel_usuario * 50000 < user.puntos_usuario) {
      dispatch(lvlUpUser());
      dispatch(showLevelUpScreen());
    }
    localStorage.setItem("user", JSON.stringify(user));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const intervalNumber = (3 / (gameState.coins - 0)) * 1000;
    const timer = setInterval(() => {
      if (second < gameState.coins) {
        setSecond((s) => (s = s + 1));
      } else {
        clearInterval(timer);
      }
    }, intervalNumber);
    return () => clearInterval(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [second]);

  useEffect(() => {
    if (info.current) {
      timeLine.current = gsap.timeline({ paused: false });
      timeLine.current.to(info.current, 1, {
        opacity: 1,
        y: 1080,
        ease: Power2.easeOut,
      });
      timeLine.current.to(scoreBox.current, 1, {
        opacity: 1,
        ease: Power2.easeOut,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info, scoreBox, coinsBox]);

  return (
    <div className="score__page">
      <div className="score__page_info" ref={info}>
        Tiempo terminado :D
      </div>
      <div className="mt-4 score__box" ref={scoreBox}>
        <div className="score__box_score">
          Puntuación Obtenida: {gameState.score}
        </div>
      </div>
      <div ref={coinsBox}>
        <div className="score__box_coins mt-3">Monedas Obtenidas</div>
        <div className="text-center score__box_coins_number">
          <BiCoin style={{ fontSize: "64px" }} />
          <span className="ms-4 fs-36">{second}</span>
        </div>
      </div>
    </div>
  );
};

export default React.memo(GamePuntuacion);

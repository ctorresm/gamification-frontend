import React from "react";
import { useDispatch } from "react-redux";
import { logout } from "../../../actions/auth";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import NavBarOption from "./NavBarOption";
import { useHistory } from "react-router-dom";

export const Navbar = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const handleLogoutOption = () => {
    localStorage.clear();
    dispatch(logout());
  };

  return (
    <div className="z2">
      <Box
        component={Grid}
        container
        boxShadow={3}
        className="navbar__main fixed"
        alignItems="center"
        justifyContent="center"
      >
        <div
          className="navbar__isotipo"
          onClick={() => {
            history.push("/");
          }}
        >
          {/* <img height="42px" src="/images/logo.png" alt="logo" /> */}
          <span className="portal__isotipo px-3">B Mates</span>
        </div>

        <NavBarOption
          type={"home"}
          text={"Inicio"}
          route={"inicio"}
        ></NavBarOption>
        <NavBarOption
          type={"store"}
          text={"Tienda"}
          route={"tienda"}
        ></NavBarOption>
        <NavBarOption
          type={"chart"}
          text={"Ranking"}
          route={"ranking"}
        ></NavBarOption>
        <NavBarOption
          type={"friends"}
          text={"Mis amigos"}
          route={"amigos"}
        ></NavBarOption>

        <button className="navbar__logout" onClick={handleLogoutOption}>
          Cerrar sesión
        </button>
      </Box>
    </div>
  );
};

import React from 'react';
import styled, { css, keyframes } from 'styled-components';

const spin = (props) => keyframes`
100% { background-position: ${props.wideWidth};  }
`;

const animation = (props) => {
  return css`
    ${(props) => spin(props)} 3.5s steps(${props.steps}) infinite;
  `;
};

const Animated = styled.div`
  background-image: url(${(props) => props.path});
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  animation: ${(props) => animation(props)};
  transform: scale(1.5);
`;

const StoreItem = (props) => {
  const { skin } = props;
  return (
    <div>
      <Animated
        wideWidth={skin.wideWidth}
        path={skin.path}
        width={skin.width}
        height={skin.height}
        steps={skin.steps}
      ></Animated>
    </div>
  );
};

export default StoreItem;

import { ClickAwayListener, Popper } from "@material-ui/core";
import React from "react";
import {
  FaDivide,
  FaLightbulb,
  FaMinus,
  FaPlus,
  FaTimes,
  FaXbox,
} from "react-icons/fa";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { getNoteById } from "../../../actions/notes";

export const HomeActivity = (props) => {
  const { name, icon, id, percentage } = props;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const dispatch = useDispatch();
  const history = useHistory();

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const clickAwayHandler = () => {
    setAnchorEl(null);
  };

  const notesHandlerClick = () => {
    dispatch(getNoteById(id));
  };

  const gameHandlerClick = () => {
    history.push("game/1");
  };

  const open = Boolean(anchorEl);
  const popperId = open ? id : undefined;

  const components = {
    divide: FaDivide,
    plus: FaPlus,
    minus: FaMinus,
    start: FaLightbulb,
    times: FaTimes,
  };

  const IconSelected = components[icon] || FaXbox;

  const renderPercentage = () => {
    let percent = "";
    switch (percentage) {
      case "p0":
        percent = " 0%";
        break;
      case "p90":
        percent = " 25%";
        break;
      case "p180":
        percent = " 50%";
        break;
      case "p270":
        percent = " 75%";
        break;
      case "p360":
        percent = " 100%";
        break;
      default:
        break;
    }
    return percent;
  };

  return (
    <div className="home__activity flex-v-center-column mt-4 mx-4">
      <ClickAwayListener onClickAway={clickAwayHandler}>
        <div
          aria-describedby={popperId}
          className="home_activity_icon flex-v-center"
          onClick={handleClick}
        >
          <div className={`home_activity_progress ${percentage || "p90"}`}>
            <div className="home__activity_img flex-v-center">
              <IconSelected className="fs-26" />
            </div>
          </div>
        </div>
      </ClickAwayListener>

      <Popper id={popperId} open={open} anchorEl={anchorEl}>
        <div className="home__activity_popper flex-v-center-column mt-2 animate__animated animate__fadeIn animate2">
          <div className="mt-2 z2">
            Actividad al
            {renderPercentage()}
          </div>

          <button
            className="w-100 mt-3 buttonSecondary"
            onClick={notesHandlerClick}
          >
            Ver notas
          </button>
          {id ? (
            <button
              className="w-100 mt-2 buttonPrimary"
              onClick={gameHandlerClick}
            >
              ¡Jugar!
            </button>
          ) : null}
        </div>
      </Popper>

      <div className="mt-2">{name}</div>
    </div>
  );
};

import React, { useEffect } from 'react';
import { Grid, LinearProgress } from '@material-ui/core';
import { BiCoinStack, BiRefresh } from 'react-icons/bi';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { niveles } from '../../../assets/niveles';

const HomeScreenProfile = ({ storeMode = false }) => {
  const { user } = useSelector((state) => state.auth);
  const { actualCoins } = useSelector((state) => state.coins);
  const [progress, setProgress] = React.useState(0);

  useEffect(() => {
    setProgress(
      ((user.puntos_usuario - 50000 * (user.nivel_usuario - 1)) / 50000) * 100
    );
  }, [user]);

  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      alignContent="center"
      direction="column"
      className="py-3"
    >
      <Grid item className="fs-20">
        {!storeMode && `¡Hola!,`}
        <b>{user.nombre_usuario}</b>
      </Grid>
      <Grid item className="home__mail_profile_photo pt-2">
        <img
          width="80px"
          height="80px"
          src="/images/default.png"
          alt="Imagen Perfil"
        />
        <button className="home__mail_change_photo">
          <BiRefresh />
        </button>
      </Grid>
      <NavLink className="fs-14 pt-2 pointer" to="/home/perfil">
        Ver mi perfil
      </NavLink>

      <Grid
        item
        container
        alignItems="center"
        alignContent="center"
        justify="center"
        direction="column"
        className="mt-4"
      >
        Mis monedas
        <div className="home__coins flex-v-center">
          <BiCoinStack className="fs-22" />
          <b className="ms-2 fs-20">{actualCoins}</b>
          <span className="fs-18 ms-2 mt-1">$</span>
        </div>
      </Grid>
      {!storeMode && (
        <Grid item container alignItems="center" className="mt-4">
          <Grid item lg={1}>
            {' '}
          </Grid>
          <Grid
            container
            item
            lg={2}
            className="flex-v-center"
            direction="column"
          >
            <img
              src={`/levels/${user.nivel_usuario}.gif`}
              className="home__icon_from"
              width="32px"
              alt=""
            />
            Level {user.nivel_usuario}
          </Grid>

          <Grid item lg={6}>
            <strong>{user.puntos_usuario}</strong> /{' '}
            {niveles[user.nivel_usuario].points}
            <div className="mb-4 w-100">
              <LinearProgress variant="determinate" value={progress} />
            </div>
          </Grid>
          <Grid
            container
            item
            lg={2}
            className="flex-v-center"
            direction="column"
          >
            <img
              src={`/levels/${user.nivel_usuario + 1}.gif`}
              className="home__icon_to"
              width="32px"
              alt=""
            />
            Level {user.nivel_usuario + 1}
          </Grid>
          <Grid item lg={1}>
            {' '}
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

export default HomeScreenProfile;

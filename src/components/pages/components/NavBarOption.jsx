import React from "react";
import { BiHomeAlt, BiPlanet, BiStore, BiTrophy } from "react-icons/bi";
import { NavLink } from "react-router-dom";

const components = {
  store: BiStore,
  home: BiHomeAlt,
  chart: BiTrophy,
  friends: BiPlanet,
};

const NavBarOption = (props) => {
  const { type, text, route } = props;
  const IconSelected = components[type];
  return (
    <NavLink
      className={`px-3 py-2 flex-v-center mx-2 navbar__item`}
      activeClassName="active"
      onClick={props.onClick}
      to={route}
    >
      <IconSelected className="me-2 fs-30" /> {text}
    </NavLink>
  );
};

export default NavBarOption;

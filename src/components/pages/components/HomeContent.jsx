import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { HomeActivity } from './HomeActivity';
import NoteScreen from '../NoteScreen';
import { GameScreen } from '../GameScreen';

const HomeContent = () => {
  const noteState = useSelector((state) => state.note);
  const gameState = useSelector((state) => state.game);

  const ComponentToShow = noteState.active
    ? NoteScreen
    : gameState.active
    ? GameScreen
    : null;

  return (
    <>
      <div className="w-100 m-4 py-4 d-flex flex-column text-center px-5">
        {noteState.active || gameState.active ? (
          <ComponentToShow />
        ) : (
          <Fragment>
            <hr />
            <h3 className="mt-2">Unidad 1</h3>
            <hr />

            <HomeActivity
              name="Inicio"
              icon="start"
              id={null}
              percentage={'p360'}
            />

            <div className="d-flex flex-v-center">
              <HomeActivity
                name="Sumas"
                icon="plus"
                id={1}
                percentage={'p270'}
              />
              <HomeActivity
                name="Restas"
                icon="minus"
                id={2}
                percentage={'p180'}
              />
            </div>

            <HomeActivity
              name="Multiplicaciones"
              icon="times"
              id={3}
              percentage={'p90'}
            />
            <HomeActivity
              name="Quebrados"
              icon="divide"
              id={4}
              percentage={'p0'}
            />

            <div className="my-3"></div>

            <hr />
            <h3 className="mt-2">Unidad 2</h3>
            <hr />

            <HomeActivity
              name="Operaciones combinadas"
              icon=""
              id={5}
              percentage={'p0'}
            />
            <HomeActivity name="Quebrados" icon="" id={6} percentage={'p0'} />
          </Fragment>
        )}
      </div>
    </>
  );
};

export default HomeContent;

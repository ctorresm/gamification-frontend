import React, { useState } from 'react';
import { BiCoinStack } from 'react-icons/bi';
import { useDispatch, useSelector } from 'react-redux';
import { buyItem, updateItem } from '../../../actions/avatar';
import { CircularProgress, Grid, Paper } from '@material-ui/core';
import StoreItem from './../components/StoreItem';

const StoreComponent = (props) => {
  const { itemsUpdated, setItemsUpdated } = props;
  const avatar = useSelector((state) => state.avatar);
  const auth = useSelector((state) => state.auth);
  const ui = useSelector((state) => state.ui);
  const { actualCoins } = useSelector((state) => state.coins);
  const dispatch = useDispatch();
  const [category, setCategory] = useState(0);

  const clickItemHandler = (item, category) => {
    switch (category) {
      case 0:
        setItemsUpdated({
          ...itemsUpdated,
          head: item,
        });
        break;
      case 1:
        setItemsUpdated({
          ...itemsUpdated,
          body: item,
        });
        break;
      case 2:
        setItemsUpdated({
          ...itemsUpdated,
          mascota: item,
        });
        break;
      case 3:
        setItemsUpdated({
          ...itemsUpdated,
          fondo: item,
        });
        break;
      default:
        break;
    }
  };

  const buyItemHandler = (item) => {
    if (!isBought(item)) {
      // eslint-disable-next-line no-restricted-globals
      const confirmedItem = confirm('Estás seguro de comprar el item?');
      if (confirmedItem) {
        dispatch(buyItem(item, auth.user.id_usuario, actualCoins));
      }
    } else {
      dispatch(updateItem(item, auth.user.id_usuario));
    }
  };

  const isBought = (el) => {
    let buyed = false;
    switch (category) {
      case 0:
        const headsBought = avatar.inventory.head.map((h) => h.id_producto);
        buyed = headsBought.includes(el.id);
        break;
      case 1:
        const bodiesBought = avatar.inventory.body.map((b) => b.id_producto);
        buyed = bodiesBought.includes(el.id);
        break;
      case 2:
        const mascotasBought = avatar.inventory.mascota.map(
          (m) => m.id_producto
        );
        buyed = mascotasBought.includes(el.id);
        break;
      case 3:
        const fondosBought = avatar.inventory.fondo.map((m) => m.id_producto);
        buyed = fondosBought.includes(el.id);
        break;
      default:
        break;
    }
    return buyed;
  };

  const isSelected = (el) => {
    let buyed = true;
    switch (category) {
      case 0:
        if (itemsUpdated) {
          if (itemsUpdated.head?.id !== el.id) {
            buyed = false;
          }
        } else {
          if (avatar.actualAvatar.head?.id_producto !== el.id) {
            buyed = false;
          }
        }
        break;
      case 1:
        if (itemsUpdated) {
          if (itemsUpdated.body?.id !== el.id) {
            buyed = false;
          }
        } else {
          if (avatar.actualAvatar.body?.id_producto !== el.id) {
            buyed = false;
          }
        }
        break;
      case 2:
        if (itemsUpdated) {
          if (itemsUpdated.mascota?.id !== el.id) {
            buyed = false;
          }
        } else {
          if (avatar.actualAvatar.mascota?.id_producto !== el.id) {
            buyed = false;
          }
        }
        break;
      case 3:
        if (itemsUpdated) {
          if (itemsUpdated.fondo?.id !== el.id) {
            buyed = false;
          }
        } else {
          if (avatar.actualAvatar.fondo?.id_producto !== el.id) {
            buyed = false;
          }
        }
        break;
      default:
        break;
    }
    return buyed;
  };

  const RenderItems = React.memo((props) => {
    let items = [];
    switch (category) {
      case 0:
        items = avatar.store.head;
        break;
      case 1:
        items = avatar.store.body;
        break;
      case 2:
        items = avatar.store.mascota;
        break;
      case 3:
        items = avatar.store.fondo;
        break;
      default:
        break;
    }

    return (
      <Grid container>
        {items.map((el, index) => {
          return (
            <Grid
              container
              item
              key={index}
              lg={4}
              md={6}
              sm={6}
              xs={12}
              justify="center"
              alignItems="center"
              direction="column"
            >
              <div
                className={`store__item MuiPaper-elevation5 flex-v-center flex-column mx-3 my-2 ${
                  isBought(el) ? 'store__item_bought' : ''
                }`}
                onClick={() => props.onClick(el, category)}
              >
                <div style={{ color: 'white' }}>{el.name}</div>
                <div
                  style={{
                    background: `${el.category === 'f' && `url(${el.path})`}`,
                  }}
                  className={`store__item_screen  flex-v-center
                  ${
                    el.price > actualCoins && !isBought(el)
                      ? 'nodisponible '
                      : ''
                  }
                  
                  
                  ${
                    isSelected(el) && el.category !== 'f'
                      ? 'store__item_selected'
                      : ''
                  }`}
                >
                  {el.category !== 'f' && <StoreItem skin={el}></StoreItem>}
                </div>
              </div>
              <button
                className={`store__item_coins flex-v-center MuiPaper-elevation2 mb-3 ${
                  isBought(el) ? 'store__item_comprado' : ''
                }`}
                onClick={() => {
                  buyItemHandler(el);
                  props.onClick(el, category);
                }}
                disabled={
                  ui.loading || (el.price > actualCoins && !isBought(el))
                    ? true
                    : false
                }
              >
                {!isBought(el) && !ui.loading ? (
                  <>
                    <BiCoinStack className="fs-22" />
                    <b className="ms-2 fs-18">{el.price || 932}</b>
                    <span className="fs-18 ms-2 mt-1">$</span>
                  </>
                ) : !ui.loading ? (
                  'Seleccionar'
                ) : (
                  <CircularProgress size={24} />
                )}
              </button>
            </Grid>
          );
        })}
      </Grid>
    );
  });

  return (
    <div>
      <Grid item>
        <Paper
          elevation={2}
          className="py-2 px-4 fs-22 flex-v-center fw-600"
          style={{ background: 'white', color: '#111' }}
        >
          Tienda 🏪
        </Paper>
      </Grid>
      <div className="mt-3 flex-v-center-left">
        <div className="MuiPaper-elevation5">
          <button
            className={`store__btnSwitch ${
              category === 0 ? 'store__btnSwitch_active' : ''
            }`}
            onClick={() => setCategory(0)}
          >
            🤯 Cabezas
          </button>
          <button
            className={`store__btnSwitch ${
              category === 1 ? 'store__btnSwitch_active' : ''
            }`}
            onClick={() => setCategory(1)}
          >
            💪 Cuerpos
          </button>
          <button
            className={`store__btnSwitch ${
              category === 2 ? 'store__btnSwitch_active' : ''
            }`}
            onClick={() => setCategory(2)}
          >
            🐾 Mascotas
          </button>
          <button
            className={`store__btnSwitch ${
              category === 3 ? 'store__btnSwitch_active' : ''
            }`}
            onClick={() => setCategory(3)}
          >
            🌄 Fondos
          </button>
        </div>
      </div>
      <Grid item container className="store__body MuiPaper-elevation5">
        <RenderItems
          category={category}
          onClick={clickItemHandler}
        ></RenderItems>
      </Grid>
    </div>
  );
};

export default React.memo(StoreComponent);

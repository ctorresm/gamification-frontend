import { Grid } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { store } from '../../../assets/store';
import styled, { css, keyframes } from 'styled-components';

const move = (props) => keyframes`
100% { background-position: ${props.wideWidth};  }
`;

const headerAnimation = (props) => {
  return css`
    ${(props) => move(props)} 3.5s steps(22) infinite;
  `;
};

const bodyAnimation = (props) => {
  return css`
    ${(props) => move(props)} 4s steps(${props.steps || 11}) infinite;
  `;
};

const mascotaAnimation = (props) => {
  return css`
    ${(props) => move(props)} 3.5s steps(${props.steps || 11}) infinite;
  `;
};

const HeaderAnimated = styled.div`
  position: absolute;
  background-image: url(${(props) => props.path});
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  animation: ${(props) => headerAnimation(props)};
  top: ${(props) => props.top};
  left: ${(props) => props.left};
`;

const BodyAnimated = styled.div`
  position: absolute;
  background-image: url(${(props) => props.path});
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  animation: ${(props) => bodyAnimation(props)};
  top: ${(props) => props.top};
`;

const MascotaAnimated = styled.div`
  position: absolute;
  background-image: url(${(props) => props.path});
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  animation: ${(props) => mascotaAnimation(props)};
  top: ${(props) => props.top};
  left: ${(props) => props.left};
`;

const HomeScreenAvatar = ({ item, mode }) => {
  const headUpdated = item?.head;
  const bodyUpdated = item?.body;
  const mascotaUpdated = item?.mascota;
  const bgUpdated = item?.fondo;

  const { actualAvatar } = useSelector((state) => state.avatar);
  const avatarActual = {
    head:
      headUpdated ||
      store.heads.filter(
        (h) => h.id === (actualAvatar?.head?.id_producto || 'h0')
      )[0],
    body:
      bodyUpdated ||
      store.body.filter(
        (b) => b.id === (actualAvatar?.body?.id_producto || 'b0')
      )[0],
    mascota:
      mascotaUpdated ||
      store.mascotas.filter(
        (m) => m.id === (actualAvatar?.mascota?.id_producto || 'm0')
      )[0],
    fondo:
      bgUpdated ||
      store.fondo.filter(
        (f) => f.id === (actualAvatar?.fondo?.id_producto || 'm0')
      )[0],
  };

  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      alignContent="center"
      direction="column"
      className="py-3"
    >
      {mode === 1 && (
        <Grid item className="fs-20">
          🤘 Mi Avatar 🤘
        </Grid>
      )}

      <Grid item className="avatar__preview mt-3 ">
        <div
          className="avatar__background"
          style={{
            backgroundImage: `url(${avatarActual?.fondo?.path || ''})`,
          }}
        ></div>

        <div className="avatar me-5 mt-3">
          <div>
            <HeaderAnimated
              wideWidth={avatarActual.head?.wideWidth}
              path={avatarActual.head.path}
              width={avatarActual.head.width}
              height={avatarActual.head.height}
              top={avatarActual.head.top}
              left={avatarActual.head.left || '0px'}
            ></HeaderAnimated>
          </div>
          <div>
            <BodyAnimated
              wideWidth={avatarActual.body?.wideWidth}
              path={avatarActual.body.path}
              width={avatarActual.body.width}
              height={avatarActual.body.height}
              top={avatarActual.body.top}
              left={avatarActual.body.left || '0px'}
              steps={avatarActual.body.steps}
            ></BodyAnimated>
          </div>
        </div>

        <div>
          <MascotaAnimated
            wideWidth={avatarActual.mascota?.wideWidth}
            path={avatarActual.mascota.path}
            width={avatarActual.mascota.width}
            height={avatarActual.mascota.height}
            top={avatarActual.mascota.top}
            left={avatarActual.mascota.left || '0px'}
            steps={avatarActual.mascota.steps}
          ></MascotaAnimated>
        </div>
      </Grid>
      {mode === 1 && (
        <NavLink className="fs-14 pt-2 pointer" to="/home/tienda">
          Ir a la tienda
        </NavLink>
      )}
    </Grid>
  );
};

export default React.memo(HomeScreenAvatar);

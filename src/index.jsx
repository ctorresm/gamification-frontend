import React from 'react';
import ReactDOM from 'react-dom';

import GamificationApp from './GamificationApp';
import './styles/styles.scss';

ReactDOM.render(<GamificationApp />, document.getElementById('root'));

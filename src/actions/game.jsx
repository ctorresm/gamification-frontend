import { store } from '../assets/store';
import { fetchSinToken } from '../helpers/fetch';
import { types } from '../types/types';

export const updateGameScore = (user, score, coins, videojuegoId) => {
  return async (dispatch) => {
    const tramaEnvio = {
      userId: user.id_usuario,
      videojuegoId: videojuegoId,
      exp: score,
      coins: coins,
    };
    const resp = await fetchSinToken(`game`, tramaEnvio, 'PUT');
    const body = await resp.json();
    if (body) {
      dispatch(updateScore(score, coins));
      dispatch(showScoreScreen(true));
    }
  };
};

export const getRanking = (id, userId) => {
  return async (dispatch) => {
    const resp = await fetchSinToken(`ranking/${id}/${userId}`);
    const body = await resp.json();
    // const dup = [...new Set(arrayId)];
    const rank = body.ranking;
    const aux = [];
    let count = 0;
    rank.map((el) => {
      if (count === 0) {
        aux.push({
          ...el,
          body: [
            {
              id_producto: el.id_producto,
              categoria_producto: el.categoria_producto,
            },
          ],
        });
        count = count + 1;
      } else {
        if (count === 4) {
          count = 1;
          aux.push({
            ...el,
            body: [
              {
                id_producto: el.id_producto,
                categoria_producto: el.categoria_producto,
              },
            ],
          });
        } else {
          aux[aux.length - 1].body.push({
            id_producto: el.id_producto,
            categoria_producto: el.categoria_producto,
          });
          count = count + 1;
        }
      }
    });

    aux.map((person) => {
      person.item = {
        body: store.body.filter((b) => b.id === person.body[0].id_producto)[0],
        fondo: store.fondo.filter(
          (f) => f.id === person.body[1].id_producto
        )[0],
        head: store.heads.filter((h) => h.id === person.body[2].id_producto)[0],
        mascota: store.mascotas.filter(
          (m) => m.id === person.body[3].id_producto
        )[0],
      };
    });

    dispatch(setRanking(aux));
  };
};

export const showScoreScreen = (showScore) => ({
  type: types.showScoreScreen,
  payload: {
    showScore,
  },
});

export const updateScore = (score, coins) => ({
  type: types.updateDataGame,
  payload: {
    coins,
    score,
  },
});

export const setGame = (id) => ({
  type: types.setGame,
  payload: {
    game: id,
  },
});

export const removeGame = () => ({
  type: types.removeGame,
});

export const setRanking = (ranking) => ({
  type: types.setRanking,
  payload: {
    ranking,
  },
});

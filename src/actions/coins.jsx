import { types } from '../types/types';

export const updateCoins = (coins) => ({
  type: types.updateCoins,
  payload: coins,
});

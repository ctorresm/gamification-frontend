import { fetchSinToken } from '../helpers/fetch';
import { types } from '../types/types';
import { removeLoading, setLoading } from './ui';
import Swal from 'sweetalert2';
import { showNotificationMini } from '../helpers/alert';
import { push } from 'react-router-redux';
import { setActualAvatar, setInventory } from './avatar';
import { updateCoins } from './coins';

export const startLoginEmailPassword = (email, password) => {
  return async (dispatch) => {
    dispatch(push('/login'));
    dispatch(setLoading());
    const tramaEnvio = {
      user: email,
      pass: password,
    };
    const resp = await fetchSinToken('login', tramaEnvio, 'POST');
    const body = await resp.json();
    if (body.ok) {
      const user = body.user[0];

      dispatch(updateCoins(user.monedas_usuario));

      dispatch(getGamesById(user));
      showNotificationMini('Acceso correcto :v', 'success');
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Contraseña incorrecta',
        text: 'Por favor intente nuevamente',
        confirmButtonText: 'Aceptar',
      });
    }
    dispatch(removeLoading());
  };
};

export const startRegisterWithEmailPassword = (
  nombre,
  nick,
  password,
  history
) => {
  return async (dispatch) => {
    dispatch(setLoading());
    const tramaEnvio = {
      nombre,
      nick,
      pass: password,
    };
    try {
      const resp = await fetchSinToken('register', tramaEnvio, 'POST');
      const body = await resp.json();
      if (body.ok) {
        Swal.fire({
          icon: 'success',
          title: '¡Registro correcto!',
          text: 'Ya puedes acceder a nuestra plataforma',
          confirmButtonText: 'Aceptar',
          onAfterClose: () => {
            history.push('login');
          },
        });
      }
      dispatch(removeLoading());
    } catch (error) {
      dispatch(removeLoading());
    }
  };
};

export const getGamesById = (user) => {
  return async (dispatch) => {
    dispatch(setLoading());
    dispatch(updateCoins(Number(localStorage.getItem('coins'))));
    const resp = await fetchSinToken(`games/${user.id_usuario}`);
    const body = await resp.json();
    const getItems = await fetchSinToken(`avatar/${user.id_usuario}`);
    const avatar = await getItems.json();
    const getInventory = await fetchSinToken(`items/${user.id_usuario}`);
    const inventory = await getInventory.json();
    dispatch(getGames(body.games));
    dispatch(login(user));
    const actualAvatar = {
      body: avatar.user[0],
      head: avatar.user[2],
      mascota: avatar.user[3],
      fondo: avatar.user[1],
    };
    dispatch(setActualAvatar(actualAvatar));
    const myInventory = {
      body: inventory.items.cuerpos,
      head: inventory.items.cabezas,
      mascota: inventory.items.mascotas,
      fondo: inventory.items.fondos,
    };
    dispatch(setInventory(myInventory));
    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('games', JSON.stringify(body.games));
    dispatch(removeLoading());
  };
};

export const getGames = (games) => {
  return {
    type: types.games,
    payload: {
      games,
    },
  };
};

export const login = (user) => {
  return {
    type: types.login,
    payload: {
      user,
    },
  };
};

export const logout = () => {
  return {
    type: types.logout,
  };
};

export const lvlUpUser = () => {
  return {
    type: types.lvlUpUser,
  };
};

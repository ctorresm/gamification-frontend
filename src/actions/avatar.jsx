import { store } from '../assets/store';
import { fetchSinToken } from '../helpers/fetch';
import { types } from '../types/types';
import { updateCoins } from './coins';
import { removeLoading, setLoading } from './ui';

export const getStore = () => {
  return async (dispatch) => {
    const storeResponse = {
      body: store.body,
      head: store.heads,
      mascota: store.mascotas,
      fondo: store.fondo,
    };
    dispatch(setStore(storeResponse));
  };
};

export const updateItem = (itemToBuy, idUser) => {
  return async (dispatch) => {
    dispatch(setLoading());
    const tramaEnvio = {
      id_usuario: idUser,
      categoria: itemToBuy.categoria,
      id_producto: itemToBuy.id,
    };
    const resp = await fetchSinToken('updateItem', tramaEnvio, 'PUT');
    const body = await resp.json();
    dispatch(removeLoading());
    if (body.ok) {
      dispatch(getMyItems(idUser));
    } else {
      alert('Ocurrió un error');
    }
  };
};

export const buyItem = (itemToBuy, idUser, actualCoins) => {
  return async (dispatch) => {
    dispatch(setLoading());
    const tramaEnvio = {
      id_usuario: idUser,
      monedas: itemToBuy.price,
      categoria: itemToBuy.categoria,
      id_producto: itemToBuy.id,
    };
    const resp = await fetchSinToken('buy', tramaEnvio, 'POST');
    const body = await resp.json();
    dispatch(removeLoading());
    if (body.ok) {
      dispatch(getMyItems(idUser));
      dispatch(updateCoins(actualCoins - itemToBuy.price));
    } else {
      alert('Ocurrió un error');
    }
  };
};

export const getMyItems = (id_usuario) => {
  return async (dispatch) => {
    const getItems = await fetchSinToken(`avatar/${id_usuario}`);
    const avatar = await getItems.json();
    const getInventory = await fetchSinToken(`items/${id_usuario}`);
    const inventory = await getInventory.json();
    const actualAvatar = {
      body: avatar.user[0],
      head: avatar.user[2],
      mascota: avatar.user[3],
      fondo: avatar.user[1],
    };
    dispatch(setActualAvatar(actualAvatar));
    const myInventory = {
      body: inventory.items.cuerpos,
      head: inventory.items.cabezas,
      mascota: inventory.items.mascotas,
      fondo: inventory.items.fondos,
    };
    dispatch(setInventory(myInventory));
  };
};

const setStore = (inventory) => ({
  type: types.setStore,
  payload: inventory,
});

export const setActualAvatar = (inventory) => ({
  type: types.setActualAvatar,
  payload: inventory,
});

export const setInventory = (inventory) => ({
  type: types.setInventory,
  payload: inventory,
});

import { types } from "../types/types";

export const setError = (err) => ({
    type: types.uiSetError,
    payload: err
})

export const removeError = () => ({
    type: types.uiRemoveError
})

export const setLoading = () => ({
    type: types.uiStartLoading,
})

export const removeLoading = () => ({
    type: types.uiFinishLoading
})

export const showLevelUpScreen = () => ({
    type: types.uiShowLevelUp
})

export const hideLevelUpScreen = () => ({
    type: types.uiHideLevelUp
})




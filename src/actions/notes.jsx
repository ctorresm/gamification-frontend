import { types } from "../types/types";

export const getNoteById = (id) => {
    return async (dispatch) => {
        dispatch(setNote(id))
    }
}

export const setNote = (id) => ({
    type: types.setNote,
    payload: {
        note: id
    }
})

export const removeNote = () => ({
    type: types.removeNote
})



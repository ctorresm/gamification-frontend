import Swal from 'sweetalert2';
import $ from 'jquery';

export function showNotificationMini(message, iconType) {
    const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000
    });
    Toast.fire({
        title: message,
        icon: iconType
    });
    $(".swal2-popup.swal2-toast").css("background-color", "#fff");
}
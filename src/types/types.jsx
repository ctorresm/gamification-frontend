export const types = {
  login: '[Auth] Login',
  logout: '[Auth] Logout',
  games: '[Auth] Get Games',

  updateCoins: '[Coins] UpdateCoins',

  lvlUpUser: '[Auth] Update Level User',

  uiSetError: '[UI] Set Error',
  uiRemoveError: '[UI] Remove Error',

  uiStartLoading: '[UI] Start Loading',
  uiFinishLoading: '[UI] Finish Loading',

  uiShowLevelUp: '[UI] Show LevelUp',
  uiHideLevelUp: '[UI] Hide LevelUp',

  setNote: '[Note] Set Note',
  removeNote: '[Note] Remove Note',

  setGame: '[Game] Set Game',
  removeGame: '[Game] Remove Game',
  showScoreScreen: '[Game] Show Score Screen',

  sendDataGame: '[Game] Send Data Game',
  updateDataGame: '[Game] Update Data Game',

  setRanking: '[Ranking] Set Ranking',

  setActualAvatar: '[Ava] Set Actual Avatar',
  setInventory: '[Ava] Set Invetory Items',
  setStore: '[Ava] Set Store Items',
};

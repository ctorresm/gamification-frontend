import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { authReducer } from '../reducers/authReducer';
import { uiReducer } from '../reducers/uiReducer';
import { notesReducer } from '../reducers/notesReducer';

import ReduxThunk from 'redux-thunk';
import { gamesReducer } from '../reducers/gameReducer';
import { avatarReducer } from '../reducers/avatarReducer';
import { coinsReducer } from '../reducers/coinsReducer';

const composeEnhancers =
  (typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

const reducers = combineReducers({
  auth: authReducer,
  ui: uiReducer,
  note: notesReducer,
  game: gamesReducer,
  avatar: avatarReducer,
  coins: coinsReducer,
});

export const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(ReduxThunk))
);

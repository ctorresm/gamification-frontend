import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { LoginScreen } from '../components/auth/LoginScreen';
import { PortalScreen } from '../components/auth/PortalScreen';
import { RegisterScreen } from '../components/auth/RegisterScreen';

export const AuthRouter = () => {
    return (
        <Switch>
            <Route
                exact
                path="/"
                component={PortalScreen}
            />

            <Route
                exact
                path="/login"
                component={LoginScreen}
            />

            <Route
                exact
                path="/register"
                component={RegisterScreen}
            />
            <Redirect to="/login" />
        </Switch>
    )
}

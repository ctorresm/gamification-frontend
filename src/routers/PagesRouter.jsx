import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Navbar } from '../components/pages/components/Navbar';
import FriendsScreen from '../components/pages/FriendsScreen';
import { GameScreen } from '../components/pages/GameScreen';
import HomeScreen from '../components/pages/HomeScreen';
import RankingScreen from '../components/pages/RankingScreen';
import StoreScreen from '../components/pages/StoreScreen';

export const PagesRouter = () => {
    return (
        <div className="pages__page">

            <Navbar />
            <Switch>
                <Route
                    exact
                    path="/home/inicio"
                    component={HomeScreen}
                />
                <Route
                    exact
                    path="/home/ranking"
                    component={RankingScreen}
                />
                <Route
                    exact
                    path="/home/tienda"
                    component={StoreScreen}
                />
                <Route
                    exact
                    path="/home/amigos"
                    component={FriendsScreen}
                />
                <Route
                    exact
                    path="/home/game/:id"
                    component={GameScreen}
                />
                <Redirect to="/home/inicio" />
            </Switch>
        </div>
    )
}

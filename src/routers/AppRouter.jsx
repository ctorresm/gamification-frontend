import React, { useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Redirect
} from 'react-router-dom';

import { AuthRouter } from './AuthRouter';
import { PagesRouter } from './PagesRouter';
import { PrivateRoute } from './PrivateRoute';
import { useDispatch, useSelector } from 'react-redux';
import { PublicRoute } from './PublicRoute';
import { getGames, getGamesById, login, logout } from '../actions/auth';

export const AppRouter = () => {

    const dispatch = useDispatch();
    const stateAuth = useSelector(state => state.auth);
    const [checking, setChecking] = useState(true);

    useEffect(() => {
        const user = localStorage.getItem('user');
        const games = localStorage.getItem('games');
        if (user) {
            dispatch(login(JSON.parse(user), true));
            dispatch(getGames(JSON.parse(games)));
            dispatch(getGamesById(JSON.parse(user)));
        } else {
            dispatch(logout());
        }
        setChecking(false);
    }, [dispatch, setChecking])

    if (checking) {
        return (<h1>Espere....</h1>)
    }

    return (
        <Router >
            <div>
                <Switch>
                    <PrivateRoute
                        isAuthenticated={!!stateAuth.user}
                        path="/home"
                        component={PagesRouter}
                    />

                    <PublicRoute
                        isAuthenticated={!!stateAuth.user}
                        path="/"
                        component={AuthRouter}
                    />

                    <Redirect to="/" />

                </Switch>
            </div>
        </Router>
    )
}
